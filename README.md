## YATZE REFACTORING KATA

Dans le cadre du [Yatzy-Refactoring-Kata](https://github.com/emilybache/Yatzy-Refactoring-Kata) , on demande de 
"refactorer" un code permettant de **calculer le score** qu'un lancer de dés obtiendrais dans **une catégorie donnée** :
par exemple Pair, Yatze ou Full.

On souhaite créer une méthode qui en entrée prend un **lancé de 5 dés** et un **type de catégorie** et qui calcul
le **score obtenu** (celui-ci peut-être égal à 0 si le lancé ne correspond pas à la catégorie). 

## Constat et solution choisie

On perçoit plusieurs limites au code donné :
- Les règles de nomanclatures et bonnes pratiques ne sont pas appliquées. Le code n'est donc pas lisible facilement et 
peu compréhensible. 
- On observe un couplage fort avec l'ensemble des méthodes placées dans la **même classe**. Cela rend difficile la réutilisation
des méthodes individuelles et augmente la complexité du code. 
- Enfin on retrouve une duplication du code, qui va à l'encontre du Clean Code. Cette duplication induit
une augmentation de la difficulté de maintenance. 

Pour permettre d'améliorer ce code, on utilise le pattern **strategy** par le biais de l'interface `CategoryCalculator`. 

Les catégories sont quant à elles placées dans une `enum` qui prend en paramètre le **calculateur correspondant**. 

Pour les cas particuliers (des suites ou des ones, twos...) et pour **éviter une duplication** du code, on décide 
d'utiliser l'abstraction. 

Le process d'écriture des méthodes de calculs de score des différentes catégories a été fait en **TDD**. 

On a ensuite un service : `CategoryPointService` qui déclare une méthode 
`calculatePointsOnCategory(DiceRoll diceRoll, Category category)` qui appelle la méthode `calculateScore` de l'implémentation 
de l'interface correspondant à la catégorie donnée en paramètre. 

Enfin, dans le `main` on "simule" un **lancé de dés** et le choix d'une **catégorie par un utilisateur** et on renvoie le résultat
du score que l'on **affiche dans la console**.