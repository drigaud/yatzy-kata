package org.com.yatzy.model;

import org.com.yatzy.calculator.CategoryCalculator;
import org.com.yatzy.calculator.impl.SameNumberCategoryCalculator;
import org.com.yatzy.calculator.impl.TwoPairsCategoryCalculator;
import org.com.yatzy.calculator.impl.multiple.FourOfAKindCategoryCalculator;
import org.com.yatzy.calculator.impl.straight.SmallStraightCategoryCalculator;
import org.com.yatzy.calculator.impl.ChanceCategoryCalculator;
import org.com.yatzy.calculator.impl.FullHouseCategoryCalculator;
import org.com.yatzy.calculator.impl.straight.LargeStraightCategoryCalculator;
import org.com.yatzy.calculator.impl.multiple.PairCategoryCalculator;
import org.com.yatzy.calculator.impl.multiple.ThreeOfAKindCategoryCalculator;
import org.com.yatzy.calculator.impl.YatzyCategoryCalculator;

public enum Category {
    CHANCE(new ChanceCategoryCalculator()),
    YATZY(new YatzyCategoryCalculator()),
    ONES(new SameNumberCategoryCalculator(Dice.ONE)),
    TWOS(new SameNumberCategoryCalculator(Dice.TWO)),
    THREES(new SameNumberCategoryCalculator(Dice.THREE)),
    FOURS(new SameNumberCategoryCalculator(Dice.FOUR)),
    FIVES(new SameNumberCategoryCalculator(Dice.FIVE)),
    SIXES(new SameNumberCategoryCalculator(Dice.SIX)),
    PAIR(new PairCategoryCalculator()),
    TWO_PAIRS(new TwoPairsCategoryCalculator()),
    THREE_OF_A_KIND(new ThreeOfAKindCategoryCalculator()),
    FOUR_OF_A_KIND(new FourOfAKindCategoryCalculator()),
    SMALL_STRAIGHT(new SmallStraightCategoryCalculator()),
    LARGE_STRAIGHT(new LargeStraightCategoryCalculator()),
    FULL_HOUSE(new FullHouseCategoryCalculator());

    private final CategoryCalculator categoryCalculator;

    Category(CategoryCalculator categoryCalculator) {
        this.categoryCalculator = categoryCalculator;
    }

    public CategoryCalculator getCategoryCalculator() {
        return categoryCalculator;
    }
}
