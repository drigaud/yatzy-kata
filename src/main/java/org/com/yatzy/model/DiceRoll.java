package org.com.yatzy.model;

import java.util.List;

public record DiceRoll(List<Dice> dices) {
    public DiceRoll {
        if (dices == null || dices.size() != 5) {
            throw new IllegalArgumentException("Number of dices in dice roll should be 5.");
        }
    }
}
