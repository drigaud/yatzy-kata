package org.com.yatzy;

import org.com.yatzy.model.Category;
import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;
import org.com.yatzy.service.CategoryPointService;

import java.util.List;

public class YatzyApplication {
    public static void main(String[] args) {
        // Given roll and category
        var roll = new DiceRoll(List.of(Dice.THREE, Dice.TWO, Dice.SIX, Dice.FOUR, Dice.TWO));
        var category = Category.TWOS;

        CategoryPointService calculator = new CategoryPointService();

        var score = calculator.calculatePointsOnCategory(roll, category);

        System.out.println(
                StringTemplate.STR.
                        "\n The score for your roll \{roll.dices().toString()} placed on \{category.toString()} is \{score}."
        );
    }
}
