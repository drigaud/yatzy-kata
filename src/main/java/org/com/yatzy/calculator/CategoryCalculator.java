package org.com.yatzy.calculator;

import org.com.yatzy.model.DiceRoll;

public interface CategoryCalculator {
    int calculateScore(DiceRoll diceRoll);
}
