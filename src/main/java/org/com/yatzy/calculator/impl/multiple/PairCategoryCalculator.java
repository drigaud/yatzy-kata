package org.com.yatzy.calculator.impl.multiple;

public class PairCategoryCalculator extends OccurrenceCategoryCalculator {
    private static final int PAIR = 2;
    public PairCategoryCalculator() {
        super(PAIR);
    }
}
