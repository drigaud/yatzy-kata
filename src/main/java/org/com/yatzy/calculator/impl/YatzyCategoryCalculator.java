package org.com.yatzy.calculator.impl;

import org.com.yatzy.calculator.CategoryCalculator;
import org.com.yatzy.model.DiceRoll;

public class YatzyCategoryCalculator implements CategoryCalculator {

    public static final int YATZE_SCORE = 50;

    @Override
    public int calculateScore(DiceRoll diceRoll) {
        return diceRoll.dices().stream().distinct().toList().size() == 1 ? YATZE_SCORE : 0;
    }
}
