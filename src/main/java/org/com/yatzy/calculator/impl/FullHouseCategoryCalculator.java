package org.com.yatzy.calculator.impl;

import org.com.yatzy.calculator.CategoryCalculator;
import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;

import java.util.Map;
import java.util.stream.Collectors;

public class FullHouseCategoryCalculator implements CategoryCalculator {
    @Override
    public int calculateScore(DiceRoll diceRoll) {
        Map<Integer, Long> countByDice = diceRoll
                .dices()
                .stream()
                .collect(Collectors.groupingBy(Dice::getValue, Collectors.counting()));

        if (countByDice.size() != 2 || !countByDice.containsValue(2L) && !countByDice.containsValue(3L)) {
            return 0;
        }

        return diceRoll.dices().stream().mapToInt(Dice::getValue).sum();
    }
}
