package org.com.yatzy.calculator.impl.multiple;

import org.com.yatzy.calculator.CategoryCalculator;
import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;

import java.util.Comparator;
import java.util.Optional;

import static org.com.yatzy.util.Utils.checkDiceOccursMoreThanGivenNumberOfTimes;

public abstract class OccurrenceCategoryCalculator implements CategoryCalculator {
    final int numberOfOccurrences;

    protected OccurrenceCategoryCalculator(int numberOfOccurrences) {
        this.numberOfOccurrences = numberOfOccurrences;
    }

    @Override
    public int calculateScore(DiceRoll diceRoll) {
        Optional<Integer> givenOccurrences = diceRoll.dices().stream()
                .filter(dice -> checkDiceOccursMoreThanGivenNumberOfTimes(diceRoll, dice, numberOfOccurrences))
                .map(Dice::getValue)
                .distinct().max(Comparator.naturalOrder());

        return givenOccurrences.orElse(0) * numberOfOccurrences;

    }
}
