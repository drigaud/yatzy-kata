package org.com.yatzy.calculator.impl.straight;

import org.com.yatzy.calculator.CategoryCalculator;
import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;

import java.util.List;

import static org.com.yatzy.util.Utils.checkDiceOccursExactlyOnce;

public abstract class StraightCategoryCalculator implements CategoryCalculator {
    final List<Dice> straight;
    final int point;

    protected StraightCategoryCalculator(List<Dice> straight, int point) {
        this.straight = straight;
        this.point = point;
    }

    @Override
    public int calculateScore(DiceRoll diceRoll) {
        if (!straight.stream().allMatch(dice -> checkDiceOccursExactlyOnce(diceRoll, dice))) {
            return 0;
        }

        return point;
    }
}
