package org.com.yatzy.calculator.impl.multiple;

public class ThreeOfAKindCategoryCalculator extends OccurrenceCategoryCalculator {
    private static final int THREE_OF_A_KIND = 3;

    public ThreeOfAKindCategoryCalculator() {
        super(THREE_OF_A_KIND);
    }
}
