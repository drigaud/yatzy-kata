package org.com.yatzy.calculator.impl.straight;

import org.com.yatzy.model.Dice;

import java.util.List;

public class LargeStraightCategoryCalculator extends StraightCategoryCalculator {
    private static final List<Dice> LARGE_STRAIGHT = List.of(Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE, Dice.SIX);
    private static final int LARGE_STRAIGHT_POINT = 20;

    public LargeStraightCategoryCalculator() {
        super(LARGE_STRAIGHT, LARGE_STRAIGHT_POINT);
    }
}
