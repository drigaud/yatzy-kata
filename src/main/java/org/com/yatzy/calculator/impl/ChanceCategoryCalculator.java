package org.com.yatzy.calculator.impl;

import org.com.yatzy.calculator.CategoryCalculator;
import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;

public class ChanceCategoryCalculator implements CategoryCalculator {
    @Override
    public int calculateScore(DiceRoll diceRoll) {
        return diceRoll.dices().stream().mapToInt(Dice::getValue).sum();
    }
}
