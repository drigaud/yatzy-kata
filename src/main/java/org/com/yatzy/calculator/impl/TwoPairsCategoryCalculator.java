package org.com.yatzy.calculator.impl;

import org.com.yatzy.calculator.CategoryCalculator;
import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;

import java.util.List;

import static org.com.yatzy.util.Utils.checkDiceOccursMoreThanGivenNumberOfTimes;

public class TwoPairsCategoryCalculator implements CategoryCalculator {
    @Override
    public int calculateScore(DiceRoll diceRoll) {
        List<Integer> pairs = diceRoll.dices().stream()
                .filter(dice -> checkDiceOccursMoreThanGivenNumberOfTimes(diceRoll, dice, 2))
                .map(Dice::getValue)
                .distinct()
                .toList();

        if (pairs.size() < 2) {
            return 0;
        }

        return pairs.stream().map(p -> p * 2).mapToInt(Integer::intValue).sum();
    }
}
