package org.com.yatzy.calculator.impl.straight;

import org.com.yatzy.model.Dice;

import java.util.List;

public class SmallStraightCategoryCalculator extends StraightCategoryCalculator {
    private static final List<Dice> SMALL_STRAIGHT = List.of(Dice.ONE, Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE);
    private static final int SMALL_STRAIGHT_POINT = 15;

    public SmallStraightCategoryCalculator() {
        super(SMALL_STRAIGHT, SMALL_STRAIGHT_POINT);
    }
}
