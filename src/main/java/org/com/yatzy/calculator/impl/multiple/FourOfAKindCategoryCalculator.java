package org.com.yatzy.calculator.impl.multiple;

public class FourOfAKindCategoryCalculator extends OccurrenceCategoryCalculator {
    private static final int FOUR_OF_A_KIND = 4;
    public FourOfAKindCategoryCalculator() {
        super(FOUR_OF_A_KIND);
    }
}