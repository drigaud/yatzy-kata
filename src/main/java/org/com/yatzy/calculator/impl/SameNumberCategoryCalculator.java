package org.com.yatzy.calculator.impl;

import org.com.yatzy.calculator.CategoryCalculator;
import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;

public class SameNumberCategoryCalculator implements CategoryCalculator {
    Dice type;
    public SameNumberCategoryCalculator(Dice type) {
        this.type = type;
    }
    int calculateScore(DiceRoll diceRoll, Dice type) {
        return diceRoll.dices()
                .stream()
                .filter(d -> d.equals(type))
                .mapToInt(Dice::getValue)
                .sum();
    }
    @Override
    public int calculateScore(DiceRoll diceRoll) {
        return calculateScore(diceRoll, type);
    }
}
