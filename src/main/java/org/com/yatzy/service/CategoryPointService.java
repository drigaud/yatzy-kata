package org.com.yatzy.service;

import org.com.yatzy.model.Category;
import org.com.yatzy.model.DiceRoll;

public class CategoryPointService {
    public int calculatePointsOnCategory(DiceRoll diceRoll, Category category) {
        return category.getCategoryCalculator().calculateScore(diceRoll);
    }
}
