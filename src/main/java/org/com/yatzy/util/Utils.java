package org.com.yatzy.util;

import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;

public class Utils {
    public static boolean checkDiceOccursExactlyOnce(DiceRoll diceRoll, Dice dice) {
        return countOccurrences(diceRoll, dice) == 1;
    }

    public static boolean checkDiceOccursMoreThanGivenNumberOfTimes(DiceRoll diceRoll, Dice dice, int givenNumberOfTimes) {
        return countOccurrences(diceRoll, dice) >= givenNumberOfTimes;
    }

    private static long countOccurrences(DiceRoll diceRoll, Dice dice) {
        return diceRoll.dices().stream().filter(dice::equals).count();
    }
}
