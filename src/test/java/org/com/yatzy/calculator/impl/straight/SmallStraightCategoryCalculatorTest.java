package org.com.yatzy.calculator.impl.straight;

import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class SmallStraightCategoryCalculatorTest {
    private final SmallStraightCategoryCalculator smallStraightCategoryCalculator = new SmallStraightCategoryCalculator();

    @Test
    void calculate_small_straight_should_return_15() {
        DiceRoll diceRoll = new DiceRoll(List.of(Dice.ONE, Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE));
        assertThat(smallStraightCategoryCalculator.calculateScore(diceRoll)).isEqualTo(15);
    }

    @Test
    void calculate_small_straight_should_return_0_when_same_dice() {
        DiceRoll diceRoll = new DiceRoll(List.of(Dice.ONE, Dice.ONE, Dice.THREE, Dice.FOUR, Dice.FIVE));
        assertThat(smallStraightCategoryCalculator.calculateScore(diceRoll)).isZero();
    }

    @Test
    void calculate_small_straight_should_return_0_when_large_straight() {
        DiceRoll diceRoll = new DiceRoll(List.of(Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE, Dice.SIX));
        assertThat(smallStraightCategoryCalculator.calculateScore(diceRoll)).isZero();
    }

}
