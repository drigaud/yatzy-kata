package org.com.yatzy.calculator.impl;

import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class TwoPairsCategoryCalculatorTest {
    private final TwoPairsCategoryCalculator twoPairsCategory = new TwoPairsCategoryCalculator();

    @Test
    void calculate_two_pairs_should_return_the_sum_of_the_pairs() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.ONE, Dice.ONE, Dice.TWO, Dice.THREE, Dice.THREE));
        assertThat(twoPairsCategory.calculateScore(rollDices)).isEqualTo(8);
    }

    @Test
    void calculate_two_pairs_should_return_0_when_no_pairs() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.ONE, Dice.ONE, Dice.TWO, Dice.THREE, Dice.FOUR));
        assertThat(twoPairsCategory.calculateScore(rollDices)).isZero();
    }

    @Test
    void calculate_two_pairs_should_return_0_when_four_of_a_kind() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.THREE, Dice.THREE, Dice.THREE, Dice.THREE, Dice.ONE));
        assertThat(twoPairsCategory.calculateScore(rollDices)).isZero();
    }
}
