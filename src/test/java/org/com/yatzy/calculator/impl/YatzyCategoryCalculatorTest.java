package org.com.yatzy.calculator.impl;

import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class YatzyCategoryCalculatorTest {
    private final YatzyCategoryCalculator yatzyCategoryCalculator = new YatzyCategoryCalculator();

    @Test
    void calculate_yatzy_should_return_50() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.TWO, Dice.TWO, Dice.TWO, Dice.TWO, Dice.TWO));
        assertThat(yatzyCategoryCalculator.calculateScore(rollDices)).isEqualTo(50);
    }

    @Test
    void calculate_yatzy_should_return_0_when_not_a_yatzy() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.ONE, Dice.ONE, Dice.THREE, Dice.FOUR, Dice.FIVE));
        assertThat(yatzyCategoryCalculator.calculateScore(rollDices)).isZero();
    }

}
