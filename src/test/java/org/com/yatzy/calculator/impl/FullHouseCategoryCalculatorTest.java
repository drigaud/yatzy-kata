package org.com.yatzy.calculator.impl;

import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class FullHouseCategoryCalculatorTest {
    private final FullHouseCategoryCalculator fullHouseCategoryCalculator = new FullHouseCategoryCalculator();

    @Test
    void calculate_full_house_should_return_sum_of_all_dices() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.TWO, Dice.TWO, Dice.FOUR, Dice.FOUR, Dice.FOUR));
        assertThat(fullHouseCategoryCalculator.calculateScore(rollDices)).isEqualTo(16);
    }

    @Test
    void calculate_full_house_should_return_0_when_yatzy() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.FIVE, Dice.FIVE, Dice.FIVE, Dice.FIVE, Dice.FIVE));
        assertThat(fullHouseCategoryCalculator.calculateScore(rollDices)).isZero();
    }

    @Test
    void calculate_full_house_should_return_0_when_not_full_house() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.ONE, Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE));
        assertThat(fullHouseCategoryCalculator.calculateScore(rollDices)).isZero();
    }
}
