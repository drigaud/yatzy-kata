package org.com.yatzy.calculator.impl.multiple;

import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class FourOfAKindCategoryCalculatorTest {
    private final FourOfAKindCategoryCalculator fourOfAKindCategoryCalculator = new FourOfAKindCategoryCalculator();

    @Test
    void calculate_four_of_a_kind_should_return_sum_of_eligible_dices() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.TWO, Dice.TWO, Dice.TWO, Dice.TWO, Dice.FIVE));
        assertThat(fourOfAKindCategoryCalculator.calculateScore(rollDices)).isEqualTo(8);
    }

    @Test
    void calculate_four_of_a_kind_should_return_0() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.TWO, Dice.TWO, Dice.TWO, Dice.FIVE, Dice.FIVE));
        assertThat(fourOfAKindCategoryCalculator.calculateScore(rollDices)).isZero();
    }

    @Test
    void calculate_four_of_a_kind_should_return_8_when_yatzy_of_two() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.TWO, Dice.TWO, Dice.TWO, Dice.TWO, Dice.TWO));
        assertThat(fourOfAKindCategoryCalculator.calculateScore(rollDices)).isEqualTo(8);
    }
}
