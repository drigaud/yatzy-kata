package org.com.yatzy.calculator.impl.multiple;

import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
class ThreeOfAKindCategoryCalculatorTest {

    private final ThreeOfAKindCategoryCalculator threeOfAKindCategory = new ThreeOfAKindCategoryCalculator();

    @Test
    void calculate_three_of_a_kind_should_return_sum_of_eligible_dices() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.THREE, Dice.THREE, Dice.THREE, Dice.FOUR, Dice.FIVE));
        assertThat(threeOfAKindCategory.calculateScore(rollDices)).isEqualTo(9);
    }

    @Test
    void calculate_three_of_a_kind_should_return_0_when_not_eligible() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.TWO, Dice.FIVE, Dice.SIX, Dice.THREE, Dice.ONE));
        assertThat(threeOfAKindCategory.calculateScore(rollDices)).isZero();
    }

    @Test
    void calculate_three_of_a_kind_should_return_sum_of_three_occurrences_when_four_occurrence() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.THREE, Dice.THREE, Dice.THREE, Dice.THREE, Dice.FIVE));
        assertThat(threeOfAKindCategory.calculateScore(rollDices)).isEqualTo(9);
    }
}
