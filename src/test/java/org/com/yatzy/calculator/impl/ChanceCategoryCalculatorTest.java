package org.com.yatzy.calculator.impl;

import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ChanceCategoryCalculatorTest {
    private final ChanceCategoryCalculator chanceCategoryCalculator = new ChanceCategoryCalculator();

    @Test
    void calculate_chance_should_return_the_sum_of_the_dices() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.ONE, Dice.ONE, Dice.THREE, Dice.THREE, Dice.SIX));
        assertThat(chanceCategoryCalculator.calculateScore(rollDices)).isEqualTo(14);
    }

    @Test
    void calculate_chance_eligible_for_other_category_should_return_the_sum_of_dices() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.SIX, Dice.SIX, Dice.SIX, Dice.SIX, Dice.SIX));
        assertThat(chanceCategoryCalculator.calculateScore(rollDices)).isEqualTo(30);
    }
}