package org.com.yatzy.calculator.impl.multiple;

import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;
import org.com.yatzy.calculator.impl.multiple.PairCategoryCalculator;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class PairCategoryCalculatorTest {
    private final PairCategoryCalculator pairCategory = new PairCategoryCalculator();
    @Test
    void calculate_pair_should_return_sum_of_highest_pair() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.THREE, Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE));
        assertThat(pairCategory.calculateScore(rollDices)).isEqualTo(6);
    }

    @Test
    void calculate_pair_should_return_sum_of_highest_pair_when_three_occurrences() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.THREE, Dice.TWO, Dice.THREE, Dice.FOUR, Dice.THREE));
        assertThat(pairCategory.calculateScore(rollDices)).isEqualTo(6);
    }

    @Test
    void calculate_pair_should_return_0_when_no_pair() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.TWO, Dice.FIVE, Dice.SIX, Dice.THREE, Dice.ONE));
        assertThat(pairCategory.calculateScore(rollDices)).isZero();
    }

}
