package org.com.yatzy.calculator.impl;

import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class SameNumberCategoryCalculatorTest {

    @Test
    void calculate_ones_should_return_sum_of_occurrences_when_place_on_one() {
        SameNumberCategoryCalculator sameNumberCategory = new SameNumberCategoryCalculator(Dice.ONE);
        DiceRoll rollDices = new DiceRoll(List.of(Dice.ONE, Dice.ONE, Dice.FIVE, Dice.ONE, Dice.ONE));
        assertThat(sameNumberCategory.calculateScore(rollDices)).isEqualTo(4);
    }

    @Test
    void calculate_fours_should_return_sum_of_occurrences_when_place_on_four() {
        SameNumberCategoryCalculator sameNumberCategory = new SameNumberCategoryCalculator(Dice.FOUR);
        DiceRoll rollDices = new DiceRoll(List.of(Dice.ONE, Dice.ONE, Dice.FOUR, Dice.ONE, Dice.ONE));
        assertThat(sameNumberCategory.calculateScore(rollDices)).isEqualTo(4);
    }

    @Test
    void calculate_ones_should_return_0_when_place_on_one() {
        SameNumberCategoryCalculator sameNumberCategory = new SameNumberCategoryCalculator(Dice.ONE);
        DiceRoll rollDices = new DiceRoll(List.of(Dice.THREE, Dice.THREE, Dice.THREE, Dice.FOUR, Dice.FIVE));
        assertThat(sameNumberCategory.calculateScore(rollDices)).isZero();
    }

}
