package org.com.yatzy.calculator.impl.straight;

import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class LargeStraightCategoryCalculatorTest {
    private final LargeStraightCategoryCalculator largeStraightCategoryCalculator = new LargeStraightCategoryCalculator();

    @Test
    void calculate_large_straight_should_return_15() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE, Dice.SIX));
        assertThat(largeStraightCategoryCalculator.calculateScore(rollDices)).isEqualTo(20);
    }

    @Test
    void calculate_large_straight_should_return_0_when_same_dice() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.ONE, Dice.ONE, Dice.THREE, Dice.FOUR, Dice.FIVE));
        assertThat(largeStraightCategoryCalculator.calculateScore(rollDices)).isZero();
    }

    @Test
    void calculate_large_straight_should_return_0_when_small_straight() {
        DiceRoll rollDices = new DiceRoll(List.of(Dice.ONE, Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE));
        assertThat(largeStraightCategoryCalculator.calculateScore(rollDices)).isZero();
    }
}
