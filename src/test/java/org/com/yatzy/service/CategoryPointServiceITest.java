package org.com.yatzy.service;

import org.com.yatzy.model.Category;
import org.com.yatzy.model.Dice;
import org.com.yatzy.model.DiceRoll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CategoryPointServiceITest {

    private final CategoryPointService calculator = new CategoryPointService();

    @Test
    void small_straight_should_return_15_when_placed_on_small_straight() {
        DiceRoll diceRoll = new DiceRoll(List.of(Dice.ONE, Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE));
        assertThat(calculator.calculatePointsOnCategory(diceRoll, Category.SMALL_STRAIGHT)).isEqualTo(15);
    }

    @Test
    void small_straight_should_return_0_when_placed_on_large_straight() {
        DiceRoll diceRoll = new DiceRoll(List.of(Dice.ONE, Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE));
        assertThat(calculator.calculatePointsOnCategory(diceRoll, Category.LARGE_STRAIGHT)).isEqualTo(0);
    }

    @Test
    void yatzy_should_return_0_when_placed_on_large_straight() {
        DiceRoll diceRoll = new DiceRoll(List.of(Dice.ONE, Dice.ONE, Dice.ONE, Dice.ONE, Dice.ONE));
        assertThat(calculator.calculatePointsOnCategory(diceRoll, Category.LARGE_STRAIGHT)).isEqualTo(0);
    }

    @Test
    void yatzy_should_return_5_when_placed_on_ones() {
        DiceRoll diceRoll = new DiceRoll(List.of(Dice.ONE, Dice.ONE, Dice.ONE, Dice.ONE, Dice.ONE));
        assertThat(calculator.calculatePointsOnCategory(diceRoll, Category.ONES)).isEqualTo(5);
    }

    @Test
    void three_six_should_return_6_when_placed_on_pairs() {
        DiceRoll diceRoll = new DiceRoll(List.of(Dice.THREE, Dice.THREE, Dice.THREE, Dice.ONE, Dice.ONE));
        assertThat(calculator.calculatePointsOnCategory(diceRoll, Category.PAIR)).isEqualTo(6);
    }
}
